package com.example.hellobeanutils;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloBeanUtils {
	public static void main(String[] args) {
		SpringApplication.run(HelloBeanUtils.class, args);
	}
}
