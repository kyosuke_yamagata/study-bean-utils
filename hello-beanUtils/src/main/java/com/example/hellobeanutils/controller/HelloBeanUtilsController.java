package com.example.hellobeanutils.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.hellobeanutils.dto.UserDto;
import com.example.hellobeanutils.dto.UserForm;
import com.example.hellobeanutils.dto.UserFormWrapper;

@Controller
@EnableAutoConfiguration
public class HelloBeanUtilsController {
	@Autowired
	UserDto ud;
	
	@Autowired
	UserForm uf;
	
	@Autowired
	UserFormWrapper ufw;
	
	@RequestMapping("/hello")
	@ResponseBody
	public String hello(){
		
		String message;

		
		message = "<br>" + "1-0. Initialized state:" + "<br>";

		message += "ud: " + ud.toString() + "<br>" +"uf: "+ uf.toString() + "<br>" +"ufw: "+ ufw.toString()+ "<br>";
		//for debug
		System.out.println(message);

		
		message += "<br><br>" + "1-1. Copy Properties from UserDto to UserForm" + "<br>";
		
		BeanUtils.copyProperties(ud, uf);
		
		message += "ud: " + ud.toString() + "<br>" +"uf: "+ uf.toString() + "<br>" +"ufw: "+ ufw.toString()+ "<br>";
		//for debug
		System.out.println(message);
		
		message += "<br><br>" + "2-1. Copy Properties from UserForm to UserFormWrapper" + "<br>";
		message += "<font color=\"blue\">NOTE: Unchanged." + "</font color><br>";

		BeanUtils.copyProperties(uf, ufw);

		message += "ud: " + ud.toString() + "<br>" +"uf: "+ uf.toString() + "<br>" +"ufw: "+ ufw.toString()+ "<br>";
		//for debug
		System.out.println(message);

		
		message += "<br><br>" + "2-2. Copy Properties from UserFormWrapper to UserFormWrapper" + "<br>";
		message += "<font color=\"blue\">NOTE: UserForm has no Setter for additional field, but UserFormWrapper has the one." + "</font color><br>";

		//for call setAdditional method, in UserFormWrapper class
		BeanUtils.copyProperties(ufw, ufw);

		message += "ud: " + ud.toString() + "<br>" +"uf: "+ uf.toString() + "<br>" +"ufw: "+ ufw.toString()+ "<br>";
		//for debug
		System.out.println(message);
		
		//for response
		return message;
	}

}
