package com.example.hellobeanutils;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.hellobeanutils.dto.UserDto;
import com.example.hellobeanutils.dto.UserForm;
import com.example.hellobeanutils.dto.UserFormWrapper;

@Configuration
public class HelloBeanUtilsConfiguration {

	@Bean
	UserDto getUserDto() {
		return new UserDto();
	}
	
	@Bean
	UserForm getUserForm() {
		return new UserForm();
	}
	

	@Bean
	UserFormWrapper getUserFormWrapper() {
		return new UserFormWrapper();
	}

}
