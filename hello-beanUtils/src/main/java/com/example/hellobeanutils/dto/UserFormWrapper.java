package com.example.hellobeanutils.dto;

import org.springframework.beans.factory.annotation.Autowired;

public class UserFormWrapper{

	@Autowired
	UserForm uf;
	
	public void setName(String name) {
		uf.setName(name);
	}
	
	public void setAge(int age) {
		uf.setAge(age);
	}
	
	public void setAdditional(String additional) {
		uf.additional = "UserFormWrapper updated";
	}
	
	public String getName() {
		return uf.getName();
	}
	
	public int getAge() {
		return uf.getAge();
	}
	
	public String getAdditional() {
		return uf.additional;
	}
	
	@Override
	public String toString() {
		return "name:" + uf.getName() + " age:" + uf.getAge() + " additional:" + uf.additional;
	}
	
	public UserForm getUserForm() {
		return this.uf;
	}
}
