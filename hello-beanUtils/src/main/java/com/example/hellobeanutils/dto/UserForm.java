package com.example.hellobeanutils.dto;

public class UserForm{
	
	private String name = "UserForm";
	private int age = 100;
	
	//package scoped field
	String additional = "UserForm default";
	
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	@Override
	public String toString() {
		return "name:" + name + " age:" + age + " additional:" + additional;
	}

}
