package com.example.hellobeanutils.dto;

public class UserDto {

	String name = "UserDto";
	int age = -1;
	boolean isStudent = false;
	  
	public String getName() {
		return name;
	}
	  
	public int getAge() {
		return age;
	}
	  
	public boolean getIsStudent() {
		return isStudent;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setIsStudent(boolean isStudent) {
		this.isStudent = isStudent;
	}
	
	@Override
	public String toString() {
		return "name:" + name + " age:" + age + " isStudent:" + isStudent;
	}
  
}
